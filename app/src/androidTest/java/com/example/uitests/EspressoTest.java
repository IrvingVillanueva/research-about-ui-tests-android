package com.example.uitests;

import androidx.test.espresso.action.ViewActions;
import androidx.test.espresso.matcher.ViewMatchers;
import androidx.test.ext.junit.rules.ActivityScenarioRule;
import androidx.test.ext.junit.runners.AndroidJUnit4;
import androidx.test.filters.LargeTest;
import static androidx.test.espresso.Espresso.*;
import static androidx.test.espresso.assertion.PositionAssertions.*;
import static androidx.test.espresso.matcher.ViewMatchers.*;
import static androidx.test.espresso.assertion.ViewAssertions.matches;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

@RunWith(AndroidJUnit4.class)
@LargeTest
public class EspressoTest {
    @Rule
    public ActivityScenarioRule<MainActivity> activityRule =
            new ActivityScenarioRule<>(MainActivity.class);
    @Test
    public void numberIsDIsplayedBelowButton(){
        // this checks if the text view with that name exists and if it does it also checks if its displayed
        onView(withText("Number")).check(matches(isDisplayed()));
        // this checks if that element is below the button named "btn1"
        onView(withText("Number")).check(isCompletelyAbove(withId(R.id.et_n1)));
    }

    @Test
    public  void  checkNumberSum(){
        // this introduces a number to the edit text
        onView(ViewMatchers.withId(R.id.et_n1)).perform(ViewActions.typeText("5"));
        onView(ViewMatchers.withId(R.id.et_n2)).perform(ViewActions.typeText("5"));
        // performs de click action of the view
        onView(withId(R.id.btn_sum)).perform(ViewActions.click());
        String expected = "10";
        //checks if the sums is correct.
        onView(withId(R.id.tv_result)).check(matches(withText(expected)));
    }

}
