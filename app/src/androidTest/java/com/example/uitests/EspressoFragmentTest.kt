package com.example.uitests

import androidx.fragment.app.testing.FragmentScenario
import androidx.fragment.app.testing.launchFragmentInContainer
import androidx.lifecycle.Lifecycle
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.ext.junit.runners.AndroidJUnit4
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith


@RunWith(AndroidJUnit4::class)
class EspressoFragmentTest {
    private lateinit var scenario: FragmentScenario<SecondUiElementsFragment>

    @Before
    fun setup(){
        scenario = launchFragmentInContainer(themeResId = R.style.Theme_UITests)
        scenario.moveToState(Lifecycle.State.STARTED)
    }

    @Test
    fun testDisplayedElements(){
        onView(withId(R.id.tv_number)).check(matches(isDisplayed()))
        onView(withId(R.id.et_fn1)).check(matches(isDisplayed()))
        onView(withId(R.id.et_fn2)).check(matches(isDisplayed()))
        onView(withId(R.id.tv_fResult)).check(matches(isDisplayed()))
        onView(withId(R.id.btn_rf)).check(matches(isDisplayed()))
    }

    @Test
    fun testInput(){
        onView(ViewMatchers.withId(R.id.et_fn1)).perform(ViewActions.typeText("5"))
        onView(ViewMatchers.withId(R.id.et_fn2)).perform(ViewActions.typeText("5"))
        onView(withId(R.id.btn_rf)).perform(ViewActions.click())
        val expected = "10"
        onView(withId(R.id.tv_fResult)).check(matches(withText(expected)))
    }

}
