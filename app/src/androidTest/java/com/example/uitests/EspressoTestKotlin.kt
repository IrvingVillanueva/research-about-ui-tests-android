package com.example.uitests

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.ext.junit.rules.ActivityScenarioRule
import androidx.test.ext.junit.runners.AndroidJUnit4
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.hamcrest.CoreMatchers.not


@RunWith(AndroidJUnit4::class)
class EspressoTestKotlin {
    @get : Rule
    var mActivityRule = ActivityScenarioRule(MainActivity::class.java)

    @Test
    fun checkVisibility() {
        onView(withText("Number")).check(matches(isDisplayed()))
        onView(withId(R.id.tv_ndisplayed)).check(matches(not(isDisplayed())));

    }

    @Test
    fun checkNumberSum() {
        // this introduces a number to the edit text
        onView(ViewMatchers.withId(R.id.et_n1)).perform(ViewActions.typeText("5"))
        onView(ViewMatchers.withId(R.id.et_n2)).perform(ViewActions.typeText("5"))
        // performs de click action of the view
        onView(withId(R.id.btn_sum)).perform(ViewActions.click())
        val expected = "10"
        //checks if the sums is correct.
        onView(withId(R.id.tv_result)).check(matches(withText(expected)))
    }
}