package com.example.uitests

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.uitests.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        initButton()
    }

    private fun initButton(){
        binding.btnSum.setOnClickListener{
            val numberOne = binding.etN1.text.toString().toInt()
            val numberTwo =  binding.etN2.text.toString().toInt()
            val result = numberOne + numberTwo;
            binding.tvResult.text  = result.toString()

        }
    }
}