package com.example.uitests

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.uitests.databinding.FragmentSecondUiElementsBinding


class SecondUiElementsFragment : Fragment() {

    private var _binding: FragmentSecondUiElementsBinding? = null
    private val binding get() = _binding!!
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentSecondUiElementsBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.btnRf.setOnClickListener{
            val n1 =  binding.etFn1.text.toString().toInt()
            val n2 =  binding.etFn2.text.toString().toInt()
            val result = n1 + n2;
            binding.tvFResult.text  = result.toString()
        }
    }
}